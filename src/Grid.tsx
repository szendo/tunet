import React, {useMemo} from 'react';

import './Grid.css';

export type Color = "black" | "gray" | "green" | "red" | "white"

type GridProps = {
    size: number;
    highlights: Color[];
    onCellClick: (index: number) => void;
};

const Grid = ({size, highlights, onCellClick}: GridProps) => {
    const sizeArr = useMemo(() => Array.from({length: size}, (_, i) => i), [size]);

    return (
        <div className="Grid">
            {sizeArr.map(rowIndex => (
                <div key={rowIndex} className="Grid__row">
                    {sizeArr.map(colIndex => (
                        <div key={colIndex} onClick={() => onCellClick(rowIndex * size + colIndex)}
                             className={"Grid__cell Grid__cell--" + (highlights[rowIndex * size + colIndex] ?? "white")}/>
                    ))}
                </div>
            ))}
        </div>
    );
}

export default Grid;
