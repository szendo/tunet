import React, {useCallback, useEffect, useState} from 'react';
import Grid, {Color} from "./Grid";

import "./TestPanel.css";

type Phase = "GENERATE" | "SHOW" | "RECALL" | "RESULT";

function generatePattern(size: number): number[] {
    const pattern = Array.from({length: size * size}, (_, i) => i);
    const patternLength = Math.floor(size * size / 2);
    for (let i = 0; i < patternLength; i++) {
        pattern.splice(0, 0, ...pattern.splice(Math.floor(Math.random() * (pattern.length - i)) + i, 1));
    }
    return pattern.slice(0, patternLength);
}

type TestPanelProps = {
    size: number;
    onEnd: () => void;
};

const TestPanel = ({size, onEnd}: TestPanelProps) => {
    const [phase, setPhase] = useState<Phase>("GENERATE");

    const [pattern, setPattern] = useState<number[]>([]);
    const [highlights, setHighlights] = useState<Color[]>([]);

    useEffect(() => {
        if (phase !== "GENERATE") return;

        setPattern(generatePattern(size));
        setPhase("SHOW");
    }, [phase, size]);

    useEffect(() => {
        if (phase !== "SHOW") return;

        setHighlights([]);
        setTimeout(() => {
            setHighlights(Array.from({length: size * size}, (_, i) => pattern.includes(i) ? "black" : "white"));
            setTimeout(() => {
                setHighlights([]);
                setTimeout(() => {
                    const patternWithMissing = pattern.slice(1);
                    setHighlights(Array.from({length: size * size}, (_, i) => patternWithMissing.includes(i) ? "black" : "white"));

                    setPhase("RECALL");
                }, 4000);
            }, 3000);
        }, 500);

    }, [pattern, phase, size]);

    const handleCellClick = useCallback((cell: number) => {
        if (phase !== "RECALL") return;

        const indexOfCell = pattern.indexOf(cell);
        if (indexOfCell > 0) {
            return;
        } else {
            setPhase("RESULT")
            const newHighlights = highlights.slice();
            console.log(highlights)
            if (indexOfCell === 0) {
                newHighlights.splice(cell, 1, "green");
            } else {
                newHighlights.splice(cell, 1, "red")
                newHighlights.splice(pattern[0], 1, "gray");
            }
            setHighlights(newHighlights)
        }
    }, [highlights, pattern, phase]);

    return (
        <div className="TestPanel">
            <Grid size={size} highlights={highlights} onCellClick={handleCellClick}/>
            {phase === "RESULT" && (
                <form onSubmit={() => onEnd()}>
                    <button type="submit" autoFocus>End</button>
                </form>
            )}
        </div>
    );
};

export default TestPanel;
