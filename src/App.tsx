import React, {useState} from 'react';
import TestPanel from "./TestPanel";

import './App.css';

type Panel = "MENU" | "TEST";

function App() {
    const [panel, setPanel] = useState<Panel>("MENU");
    const [size, setSize] = useState(2);

    return (
        <div className="App">
            {panel === "MENU" && (
                <form onSubmit={() => setPanel("TEST")}>
                    <select value={size} autoFocus onChange={e => setSize(+e.target.value)}>
                        {[2, 3, 4, 5, 6, 7, 8].map(s => <option value={s}>{s}×{s}</option>)}
                    </select>
                    <button type="submit">Go</button>
                </form>
            )}
            {panel === "TEST" && <TestPanel size={size} onEnd={() => setPanel("MENU")}/>}
        </div>
    );
}

export default App;
